<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Dmitry
  Date: 16.02.2019
  Time: 0:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Students</title>
</head>
<body>
<%=request.getAttribute("title")%>
<br>
<a href="./groups">back</a>
<br>
<br>
<table BORDER="1" CELLPADDING="6" CELLSPACING="1">
    <tr>
        <th>Username</th>
        <th>Password</th>
        <th>Full Name</th>
        <th>Date of Birth</th>
        <th>City</th>
        <th></th>
    </tr>
    <c:forEach items="${studentList}" var="student" varStatus="status">
        <tr>
            <td>${student.getUser().getUserName()}</td>
            <td>${student.getUser().getPassword()}</td>
            <td>${student.getFullName()}</td>
            <td>${student.getDateOfBirthString()}</td>
            <td>${student.getCity()}</td>
            <td>
                <form action="./deleteStudent" method="post">
                    <input type="submit" value="Delete">
                    <input type="hidden" name="groupNumber" value="<%=request.getAttribute("groupNumber")%>">
                    <input type="hidden" name="userID" value="${student.getUser().getId()}">
                </form>
            </td>

        </tr>
    </c:forEach>
</table>
<br>
<form action="./addStudent" method="post">
    <input type="text" name="userName" placeholder="Username">
    <input type="password" name="password" placeholder="Password">
    <input type="text" name="fullName" placeholder="Full Name">
    <input type="date" name="dateOfBirth" placeholder="Date of Birth">
    <input type="text" name="city" placeholder="City">
    <input type="submit" value="Add new student">
    <input type="hidden" name="groupNumber" value="<%=request.getAttribute("groupNumber")%>">
</form>

<% if (request.getAttribute("message") != null) { %>
<%=request.getAttribute("message")%>
<% } %>
</body>
</html>
