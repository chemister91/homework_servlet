<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Dmitry
  Date: 09.02.2019
  Time: 10:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Student Groups</title>
</head>
<body>
<%=request.getAttribute("title")%>
<br>
<a href="./">back</a>
<br>
<c:forEach var="num" items="${groupList}">
    <a href="./students?groupNumber=${num}"><p>${num}</p></a>
</c:forEach>
<br>
<form action="./newGroup" method="post">
    <input type="number" name="groupNumber" placeholder="Group number">
    <input type="submit" value="Create a new group">
</form>
<% if (request.getAttribute("message") != null) { %>
<%=request.getAttribute("message")%>
<% } %>
</body>
<br>
<form action="./logout" method="post">
    <input type="submit" value="Logout">
</form>
</html>
