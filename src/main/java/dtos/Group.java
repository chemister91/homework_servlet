package dtos;

import java.util.LinkedList;
import java.util.List;

public class Group {
    private List<Student> studentList = new LinkedList<>();
    private int groupNumber;

    public Group(int groupNumber) {
        this.groupNumber = groupNumber;
    }

    public Group addStudentToGroup(Student student) {
        studentList.add(student);
        return this;
    }

    public Group deleteStudentFromGroup(Student student) {
        studentList.remove(student);
        return this;
    }

    public List<Student> getStudents() {
        return studentList;
    }

    public int getGroupNumber() {
        return groupNumber;
    }

    public void setGroupNumber(int groupNumber) {
        this.groupNumber = groupNumber;
    }

    @Override
    public String toString() {
        return String.valueOf(groupNumber);
    }
}
