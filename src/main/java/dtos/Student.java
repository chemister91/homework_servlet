package dtos;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Student {
    private User user;
    private String fullName;
    private Calendar dateOfBirth;
    private String city;

    public Student() {
    }

    public Student(User user, String fullName, Calendar dateOfBirth, String city) {
        this.user = user;
        this.fullName = fullName;
        this.dateOfBirth = dateOfBirth;
        this.city = city;
    }

    @Override
    public String toString() {
        return "Student{" +
                user.toString() +
                ", fullName='" + fullName + '\'' +
                ", dateOfBirth='" + getDateOfBirthString() + '\'' +
                ", city='" + city + '\'' +
                '}';
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Calendar getDateOfBirth() {
        return dateOfBirth;
    }

    public String getDateOfBirthString() {
        return new SimpleDateFormat("dd MMM yyyy").format(dateOfBirth.getTime());
    }

    public void setDateOfBirth(Calendar dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
