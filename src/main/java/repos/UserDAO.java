package repos;

import dtos.User;

import java.util.List;

public interface UserDAO {

    User createUser(int id, String userName, String password);

    void deleteUser(int id);

    User updateUser(int id, String userName, String password);

    User getUser(int id);

    List<User> getAllUsers();
}