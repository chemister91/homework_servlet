package repos;

import dtos.User;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserDAOSimpleImpl implements UserDAO {
    private static Map<Integer, User> users = new HashMap<>();

    public UserDAOSimpleImpl() {
    }

    @Override
    public User createUser(int id, String userName, String password) {
        return updateUser(id, userName, password);
    }

    @Override
    public void deleteUser(int id) {
        users.remove(id);
    }

    @Override
    public User updateUser(int id, String userName, String password) {
        User newUser = new User(id, userName, password);
        users.put(id, newUser);
        return newUser;
    }

    @Override
    public User getUser(int id) {
        return users.get(id);
    }

    @Override
    public List<User> getAllUsers() {
        return new ArrayList<>(users.values());
    }
}
