package repos;

import dtos.Student;

import java.util.Calendar;
import java.util.List;

public interface StudentDAO {
    Student createStudent(String userName, String password, String fullName, Calendar dateOfBirth, String city);

    Student updateStudent(int studentID, String userName, String password, String fullName, Calendar dateOfBirth, String city);

    Student getStudent(int studentID);

    void deleteStudent(int studentID);

    List<Student> getAllStudent();
}
