package repos;

import dtos.Group;

import java.util.List;

public interface GroupDAO {
    Group createGroup(int groupNumber);

    Group updateGroup(int groupNumber, Group group);

    Group getGroup(int groupNumber);

    void deleteGroup(int groupNumber);

    List<Group> getAllGroup();
}
