package repos;

import dtos.Group;

import java.util.*;

public class GroupDAOSimpleImpl implements GroupDAO {
    private static Map<Integer, Group> groups = new TreeMap<>();

    @Override
    public Group createGroup(int groupNumber) {
        return updateGroup(groupNumber, new Group(groupNumber));
    }

    @Override
    public Group updateGroup(int groupNumber, Group group) {
        groups.put(groupNumber, group);
        return group;
    }

    @Override
    public Group getGroup(int groupNumber) {
        return groups.get(groupNumber);
    }

    @Override
    public void deleteGroup(int groupNumber) {
        groups.remove(groupNumber);
    }

    @Override
    public List<Group> getAllGroup() {
        return new LinkedList<>(groups.values());
    }
}
