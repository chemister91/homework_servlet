package repos;

import dtos.Student;
import dtos.User;

import java.util.*;

public class StudentDAOSimpleImpl implements StudentDAO {
    private static Map<Integer, Student> students = new HashMap<>();
    private UserDAO userDAO;

    public StudentDAOSimpleImpl(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    @Override
    public Student createStudent(String userName, String password, String fullName, Calendar dateOfBirth, String city) {
        return updateStudent(getNewStudentID(), userName, password, fullName, dateOfBirth, city);
    }

    @Override
    public Student updateStudent(int studentID, String userName, String password, String fullName, Calendar dateOfBirth, String city) {
        User user;
        if (students.containsKey(studentID)) {
            user = students.get(studentID).getUser();
        } else {
            user = userDAO.createUser(studentID, userName, password);
        }
        Student newStudent = new Student(
                user,
                fullName,
                dateOfBirth,
                city
        );
        students.put(studentID, newStudent);
        return newStudent;
    }

    @Override
    public Student getStudent(int studentID) {
        return students.get(studentID);
    }

    @Override
    public void deleteStudent(int studentID) {
        userDAO.deleteUser(students.get(studentID).getUser().getId());
        students.remove(studentID);
    }

    @Override
    public List<Student> getAllStudent() {
        return new ArrayList<>(students.values());
    }

    private int getNewStudentID() {
        int newID = students.size();
        do {
            newID++;
        } while (students.containsKey(newID));
        return newID;
    }
}
