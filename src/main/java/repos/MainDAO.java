package repos;

import dtos.Student;
import factory.*;

public class MainDAO {
    private static MainDAO ourInstance = new MainDAO();
    private UserDAO userDAO;
    private StudentDAO studentDAO;
    private GroupDAO groupDAO;
    private Factory factory;

    public static MainDAO getInstance() {
        return ourInstance;
    }

    private MainDAO() {
        userDAO = new UserDAOSimpleImpl();
        studentDAO = new StudentDAOSimpleImpl(userDAO);
        groupDAO = new GroupDAOSimpleImpl();
    }

    public UserDAO getUserDAO() {
        return userDAO;
    }

    public StudentDAO getStudentDAO() {
        return studentDAO;
    }

    public GroupDAO getGroupDAO() {
        return groupDAO;
    }

    public void createDefaults(String factoryType) {
        switch (factoryType) {
            case "RU":
                factory = new RuFactory(studentDAO, groupDAO);
                break;
            case "EN":
            default:
                factory = new EnFactory(studentDAO, groupDAO);
                break;
        }
        Student tom = factory.createStudent(StudentTypes.TOM);
        Student sam = factory.createStudent(StudentTypes.SAM);
        Student john = factory.createStudent(StudentTypes.JOHN);
        factory.createGroup(GroupTypes.GROUP_101);
        factory.createGroup(GroupTypes.GROUP_203);
        factory.createGroup(GroupTypes.GROUP_306);
        factory.createGroup(GroupTypes.GROUP_404);
        factory.createGroup(GroupTypes.GROUP_507)
                .addStudentToGroup(tom)
                .addStudentToGroup(sam)
                .addStudentToGroup(john);
    }

    public Boolean defaultsCreated() {
       return factory != null;
    }
}
