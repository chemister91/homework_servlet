package controller;

import repos.*;
import services.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.Logger;

public class LoginController extends HttpServlet implements CustomController {
    private UserService userService;
    private MainDAO mainDAO;
    private Logger logger = Logger.getLogger(this.getClass().getName());

    public LoginController() {
        mainDAO = MainDAO.getInstance();
        userService = new UserService(mainDAO.getUserDAO());
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            switch (req.getServletPath()) {
                case URI_LOGIN:
                    login(req, resp);
                    break;
                case URI_LOGOUT:
                    logout(req, resp);
                    break;
                case URI_RU:
                case URI_EN:
                    createDefaults(req, resp);
                    break;
            }
        } catch (Exception e) {
            logger.severe(e.toString());
            req.setAttribute(ATTRIBUTE_MESSAGE, e.getMessage());
            req.getRequestDispatcher(URI_INDEX_EXT).forward(req, resp);
        }
    }

    private void login(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String userName = req.getParameter(PARAMETER_USERNAME);
        String password = req.getParameter(PARAMETER_PASSWORD);
        if (req.getSession().getAttribute(ATTRIBUTE_FACTORY) == null) {
            req.getRequestDispatcher(URI_INIT_FULL).forward(req, resp);
        } else if (userService.userIsValid(userName, password)) {
            req.getSession().setAttribute(ATTRIBUTE_LOGIN, userName.hashCode());
            resp.sendRedirect(URI_GROUPS_EXT);
        } else {
            throw new IllegalArgumentException(EXCEPTION_INVALID_USER);
        }
    }

    private void logout(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        req.getSession().setAttribute(ATTRIBUTE_LOGIN, null);
        req.getSession().setAttribute(ATTRIBUTE_FACTORY, null);
        req.getRequestDispatcher(URI_INDEX_EXT).forward(req, resp);
    }

    private void createDefaults(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        switch (req.getServletPath()) {
            case URI_RU:
                mainDAO.createDefaults("RU");
                break;
            case URI_EN:
            default:
                mainDAO.createDefaults("EN");
                break;
        }
        req.getSession().setAttribute(ATTRIBUTE_FACTORY, req.getServletPath());
        login(req, resp);
    }
}
