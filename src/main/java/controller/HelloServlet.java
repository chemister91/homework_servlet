package controller;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public class HelloServlet extends HttpServlet {
    private String testString = "";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        testString = req.getParameter("testText");
        req.setAttribute("myAttr", testString + "smth");
        req.getRequestDispatcher("/response.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        testString = req.getParameter("name");
        req.setAttribute("myAttr", testString + ", nice to see you");
        req.setAttribute("list", new ArrayList<Integer>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8)));
        req.getRequestDispatcher("/response.jsp").forward(req, resp);
    }
}
