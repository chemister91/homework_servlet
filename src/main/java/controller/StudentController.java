package controller;

import dtos.Student;
import repos.MainDAO;
import repos.StudentDAO;
import services.GroupService;
import services.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.logging.Logger;

public class StudentController extends HttpServlet implements CustomController {
    private GroupService groupService;
    private UserService userService;
    private MainDAO mainDAO;
    private Logger logger = Logger.getLogger(this.getClass().getName());

    public StudentController() {
        mainDAO = MainDAO.getInstance();
        groupService = new GroupService(mainDAO.getGroupDAO());
        userService = new UserService(mainDAO.getUserDAO());
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            int groupNumber = getGroupNumber(req);
            if (groupService.isNewGroup(groupNumber)) {
                throw new IllegalArgumentException(EXCEPTION_GROUP_NOT_EXISTS);
            } else {
                req.setAttribute(ATTRIBUTE_TITLE, String.format(ATTRIBUTE_TITLE_STUDENTS, groupNumber));
                req.setAttribute(ATTRIBUTE_GROUP_NUMBER, groupNumber);
                req.setAttribute(ATTRIBUTE_STUDENTS, groupService.getStudentListByGroup(groupNumber));
                req.getRequestDispatcher(URI_STUDENTS_FULL).forward(req, resp);
            }
        } catch (IllegalArgumentException e) {
            logger.warning(e.toString());
            resp.sendRedirect(URI_GROUPS_EXT);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            req.setCharacterEncoding("UTF-8");
            switch (req.getServletPath()) {
                case URI_ADD_STUDENT:
                    createStudent(req);
                    break;
                case URI_DELETE_STUDENT:
                    deleteStudent(req);
                    break;
            }
        } catch (Exception e) {
            logger.warning(e.toString());
            req.setAttribute(ATTRIBUTE_MESSAGE, e.getMessage());
        } finally {
            doGet(req, resp);
        }
    }

    private void createStudent(HttpServletRequest req) throws Exception {
        Calendar dateOfBirth = new GregorianCalendar();
        dateOfBirth.setTime(new SimpleDateFormat("yyyy-MM-dd").parse(req.getParameter(PARAMETER_DATE_OF_BIRTH)));
        String userName = req.getParameter(PARAMETER_USERNAME);
        String password = req.getParameter(PARAMETER_PASSWORD);
        if (userService.userExists(userName)) {
            throw new IllegalArgumentException(String.format(EXCEPTION_USER_ALREADY_EXISTS, userName));
        } else if (userName == null || userName.isEmpty() || password == null || password.isEmpty()) {
            throw new IllegalArgumentException(EXCEPTION_EMPTY_CREDENTIALS);
        } else {
            Student student = mainDAO.getStudentDAO().createStudent(
                    userName,
                    password,
                    req.getParameter(PARAMETER_FULL_NAME),
                    dateOfBirth,
                    req.getParameter(PARAMETER_CITY)
            );
            mainDAO
                    .getGroupDAO()
                    .getGroup(getGroupNumber(req))
                    .addStudentToGroup(student);
        }
    }

    private void deleteStudent(HttpServletRequest req) throws NumberFormatException {
        int userID = Integer.valueOf(req.getParameter(PARAMETER_USER_ID));
        StudentDAO studentDAO = mainDAO.getStudentDAO();
        mainDAO
                .getGroupDAO()
                .getGroup(getGroupNumber(req))
                .deleteStudentFromGroup(studentDAO.getStudent(userID));
        studentDAO.deleteStudent(userID);
    }

    private int getGroupNumber(HttpServletRequest req) throws NumberFormatException {
        return Integer.valueOf(req.getParameter(ATTRIBUTE_GROUP_NUMBER));
    }
}
