package controller;

import repos.MainDAO;
import services.GroupService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.Logger;

public class GroupController extends HttpServlet implements CustomController {
    private GroupService groupService;
    private MainDAO mainDAO;
    private Logger logger = Logger.getLogger(this.getClass().getName());

    public GroupController() {
        mainDAO = MainDAO.getInstance();
        groupService = new GroupService(mainDAO.getGroupDAO());
    }


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute(ATTRIBUTE_TITLE, ATTRIBUTE_TITLE_GROUPS);
        req.setAttribute(ATTRIBUTE_GROUP, groupService.getAllGroups());
        req.getRequestDispatcher(URI_GROUPS_FULL).forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getServletPath().equals(URI_NEW_GROUP)) {
            try {
                createNewGroup(req);
            } catch (NumberFormatException e) {
                logger.warning(e.toString());
                req.setAttribute(ATTRIBUTE_MESSAGE, EXCEPTION_WRONG_NUMBER);
            } catch (IllegalArgumentException e) {
                logger.warning(e.toString());
                req.setAttribute(ATTRIBUTE_MESSAGE, e.getMessage());
            } catch (Exception e) {
                logger.severe(e.toString());
                req.setAttribute(ATTRIBUTE_MESSAGE, EXCEPTION_ELSE);
            } finally {
                doGet(req, resp);
            }
        }
    }

    private void createNewGroup(HttpServletRequest req) {
        int groupNumber = Integer.valueOf(req.getParameter(ATTRIBUTE_GROUP_NUMBER));
        if (groupNumber <= 0) {
            throw new NumberFormatException();
        } else if (groupService.isNewGroup(groupNumber)) {
            mainDAO.getGroupDAO().createGroup(groupNumber);
        } else {
            throw new IllegalArgumentException(EXCEPTION_GROUP_ALREADY_EXISTS);
        }
    }
}
