package controller.filter;

import controller.CustomController;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class LoginFilter implements Filter, CustomController {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        HttpSession httpSession = ((HttpServletRequest) servletRequest).getSession();
        String servletPath = ((HttpServletRequest) servletRequest).getServletPath();
        List<String> acceptedUriList = Arrays.asList(URI_INDEX, URI_LOGIN, URI_EN, URI_RU);
        if (httpSession.getAttribute(ATTRIBUTE_LOGIN) != null || acceptedUriList.stream().anyMatch(uri -> uri.trim().equals(servletPath))) {
            filterChain.doFilter(servletRequest, servletResponse);
        } else {
            HttpServletResponse httpServletResponse = (HttpServletResponse) servletResponse;
            HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
            httpServletResponse.sendRedirect(httpServletRequest.getContextPath());
        }
    }

    @Override
    public void destroy() {

    }
}
