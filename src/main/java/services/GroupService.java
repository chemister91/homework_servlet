package services;

import dtos.Group;
import dtos.Student;
import repos.GroupDAO;

import java.util.List;

import static java.util.stream.Collectors.toList;

public class GroupService {
    private GroupDAO groupDAO;

    public GroupService(GroupDAO groupDAO) {
        this.groupDAO = groupDAO;
    }

    public List<String> getAllGroups() {
        return groupDAO
                .getAllGroup()
                .stream()
                .map(Group::toString)
                .collect(toList());
    }

    public List<String> getAllStudentsByGroup(int groupNumber) {
        return groupDAO
                .getGroup(groupNumber)
                .getStudents()
                .stream()
                .map(Student::toString)
                .collect(toList());
    }

    public List<Student> getStudentListByGroup(int groupNumber) {
        return groupDAO
                .getGroup(groupNumber)
                .getStudents();
    }

    public boolean isNewGroup(int groupNumber) {
        return groupDAO.getGroup(groupNumber) == null;
    }
}
