package services;

import dtos.Student;
import repos.StudentDAO;

import java.util.List;

import static java.util.stream.Collectors.toList;

public class StudentService {
    private StudentDAO studentDAO;

    public StudentService(StudentDAO studentDAO) {
        this.studentDAO = studentDAO;
    }

    public List<String> getAllStudents() {
        return studentDAO
                .getAllStudent()
                .stream()
                .map(Student::toString)
                .collect(toList());
    }
}
