package services;

import repos.UserDAO;

public class UserService {
    private UserDAO userDAO;

    public UserService(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    public Boolean userIsValid(String userName, String password) {
        return userDAO
                .getAllUsers()
                .stream()
                .anyMatch(user ->
                        checkValue(user.getUserName(), userName) && checkValue(user.getPassword(), password)
                );
    }

    public Boolean userExists(String userName) {
        return userDAO
                .getAllUsers()
                .stream()
                .anyMatch(user ->
                        checkValue(user.getUserName(), userName)
                );
    }

    private Boolean checkValue(String targetValue, String trueValue) {
        return (targetValue != null) && (trueValue != null) && (targetValue.equals(trueValue));
    }
}
