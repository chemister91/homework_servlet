package factory;

public enum StudentTypes {
    JOHN,
    TOM,
    SAM,
    OLIVER,
    JACK,
    HARRY,
    JACOB,
    THOMAS,
    JAMES,
    WILLIAM;
}
