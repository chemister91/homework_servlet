package factory;

import dtos.Group;
import dtos.Student;
import repos.GroupDAO;
import repos.StudentDAO;

import java.util.GregorianCalendar;

public class RuFactory extends Factory {
    private StudentDAO studentDAO;
    private GroupDAO groupDAO;

    public RuFactory(StudentDAO studentDAO, GroupDAO groupDAO) {
        this.studentDAO = studentDAO;
        this.groupDAO = groupDAO;
    }

    @Override
    public Student createStudent(StudentTypes studentType) {
        switch (studentType) {
            case JOHN:
                return studentDAO.createStudent(
                        "John",
                        "789",
                        "Сидоров Джон Васильевич",
                        new GregorianCalendar(1973, 5, 7),
                        "London"
                );
            case TOM:
                return studentDAO.createStudent(
                        "Tom",
                        "123",
                        "Иванов Том Иванович",
                        new GregorianCalendar(1986, 10, 28),
                        "NYC"
                );
            case JACK:
                return studentDAO.createStudent(
                        "Jack",
                        "357",
                        "Дэниэлс Джек Сергеевич",
                        new GregorianCalendar(1984, 1, 16),
                        "Paris"
                );
            case HARRY:
                return studentDAO.createStudent(
                        "Harry",
                        "951",
                        "Кейн Гарри Эдвардович",
                        new GregorianCalendar(1993, 7, 8),
                        "Brussels"
                );
            case JACOB:
                return studentDAO.createStudent(
                        "Jacob",
                        "746",
                        "Дефалонов Якоб Валерьевич",
                        new GregorianCalendar(1973, 12, 1),
                        "Brussels"
                );
            case JAMES:
                return studentDAO.createStudent(
                        "James",
                        "554",
                        "Родригес Джеймс Давидович",
                        new GregorianCalendar(1985, 2, 6),
                        "Bratislava"
                );
            case OLIVER:
                return studentDAO.createStudent(
                        "Oliver",
                        "439",
                        "Джемми Оливер Игоревич",
                        new GregorianCalendar(1949, 8, 16),
                        "Moscow"
                );
            case THOMAS:
                return studentDAO.createStudent(
                        "Thomas",
                        "391",
                        "Шелби Томас Васильевич",
                        new GregorianCalendar(1995, 12, 7),
                        "Helsinki"
                );
            case WILLIAM:
                return studentDAO.createStudent(
                        "William",
                        "319",
                        "Лоусенов Вилльям Иванович",
                        new GregorianCalendar(1979, 8, 21),
                        "Mexico"
                );
            case SAM:
            default:
                return studentDAO.createStudent(
                        "Sam",
                        "456",
                        "Петров Сэм Сергеевич",
                        new GregorianCalendar(1993, 11, 14),
                        "Moscow"
                );
        }
    }

    @Override
    public Group createGroup(GroupTypes groupType) {
        switch (groupType) {
            case GROUP_101:
                return groupDAO.createGroup(101)
                        .addStudentToGroup(createStudent(StudentTypes.HARRY));
            case GROUP_203:
                return groupDAO.createGroup(203)
                        .addStudentToGroup(createStudent(StudentTypes.JACK))
                        .addStudentToGroup(createStudent(StudentTypes.JACOB));
            case GROUP_306:
                return groupDAO.createGroup(306)
                        .addStudentToGroup(createStudent(StudentTypes.JAMES));
            case GROUP_404:
                return groupDAO.createGroup(404)
                        .addStudentToGroup(createStudent(StudentTypes.OLIVER))
                        .addStudentToGroup(createStudent(StudentTypes.THOMAS))
                        .addStudentToGroup(createStudent(StudentTypes.WILLIAM));
            case GROUP_507:
            default:
                return groupDAO.createGroup(507);
        }
    }
}
