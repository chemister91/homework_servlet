package factory;

import dtos.Group;
import dtos.Student;

public abstract class Factory {
    public abstract Student createStudent(StudentTypes studentType);

    public abstract Group createGroup(GroupTypes groupType);
}
